import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Camera } from '@ionic-native/camera';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule, Storage } from '@ionic/storage';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { Items } from '../mocks/providers/items';
import { Settings } from '../providers/providers';
import { MyApp } from './app.component';
import { Trips } from '../providers/providers';
import { Trip } from '../providers/providers';
import { Messages } from '../providers/messages/messages';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { Loader } from '../providers/loader/loader';
import { WindowService } from '../providers/windowservice/windowservice';
import { UserService } from '../providers/user/userservice';
import { ApiService } from '../providers/api/apiservice';
import { AuthService } from '../providers/auth/authservice';

const config: SocketIoConfig = { url: 'https://voyagezy-app.herokuapp.com', options: {} };

// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function provideSettings(storage: Storage) {
  /**
   * The Settings provider takes a set of default settings for your app.
   *
   * You can add new settings options at any time. Once the settings are saved,
   * these values will not overwrite the saved values (this can be done manually if desired).
   */
  return new Settings(storage, {
    option1: true,
    option2: 'Ionitron J. Framework',
    option3: '3',
    option4: 'Hello'
  });
}

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonicModule.forRoot(MyApp,{
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false,
      backButtonText: '',
      backButtonIcon: 'ios-arrow-back',
      iconMode: 'md',

    }),
    IonicStorageModule.forRoot(),
    SuperTabsModule.forRoot(),
    SocketIoModule.forRoot(config)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    Items,
    Trip,
    Trips,
    Camera,
    SplashScreen,
    StatusBar,
    { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Messages,
    Loader,
    WindowService,
    UserService,
    AuthService,
    ApiService,
  ]
})
export class AppModule { }
