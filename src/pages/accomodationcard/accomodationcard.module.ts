import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccomodationcardPage } from './accomodationcard';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AccomodationcardPage,
  ],
  imports: [
    IonicPageModule.forChild(AccomodationcardPage),
    TranslateModule.forChild(),
  ],
})
export class AccomodationcardPageModule {}
