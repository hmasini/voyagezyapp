import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController } from 'ionic-angular';
import { User } from '../../models/user';
import { Tab1Root } from '../pages';
import { Tab2Root } from '../pages';
import { Tab3Root } from '../pages';
import { UserService } from '../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root: any = Tab1Root;
  tab2Root: any = Tab2Root;
  tab3Root: any = Tab3Root;

  constructor(public navCtrl: NavController, public translate: TranslateService, public userService: UserService) {
    this.userService.getOnStorage().then((user) => {
      this.translate.use(User.ParseFromObject(user).language);
    });
  }
}
