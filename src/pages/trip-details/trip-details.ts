import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams, Slides, Scroll, FabContainer, Toolbar, Segment } from 'ionic-angular';
import { AttractioncardPage } from '../pages';
import { DirectioncardPage } from '../pages';
import { AccomodationcardPage } from '../pages';
import { EstablishmentcardPage } from '../pages';
import { DaySummaryPage } from "../pages";
import { TranslateService } from '@ngx-translate/core';
import { SuperTabs } from 'ionic2-super-tabs';
import { Loader } from '../../providers/providers';
import { UserService } from '../../providers/providers';
import { User } from '../../models/user';
import moment from 'moment';
/**
 * Generated class for the TripDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trip-details',
  templateUrl: 'trip-details.html',
})
export class TripDetailsPage {
  selectedTab = 0;
  
  @ViewChild('mySlider') slider: Slides;
  @ViewChild('mySegment') segment: Segment;
  @ViewChild('scroll') scroll: any;
  selectedSegment: any;
  slides: any;
  
  tripDetails: any;
  tripPlan: any;
  tripDays: Array <any>;
  daysArray: Array <any>;
  tripDisplay: any;
  dayIndex: number;
  currentDay: any;
  hasNext: boolean;
  hasPrevious: boolean;
  scrollOffset: number = 250;
  widthScroll: number = 1000;
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public loader: Loader, 
              public translate: TranslateService, public userService: UserService) {
    this.userService.getOnStorage().then((user) => {
      this.translate.use(User.ParseFromObject(user).language);
    });
    var tripKey = navParams.get('data');
    var trips = window.localStorage.getItem(tripKey);
    this.tripDetails  = JSON.parse(trips);
    this.selectedSegment = "0";
    this.slides = [];
    this.tripPlan = this.tripDetails.tripPlan;
    this.daysArray = [];
    var firstDay = moment(this.tripDetails.startDate);
    this.tripDays = this.tripDetails.tripPlan.days;
    this.dayIndex = 0;
    var id = 0;
    this.tripDays.forEach( day => {
      this.slides.push({id: id, day: firstDay.format("DD/MM")});
      id += 1;
      day.totalCost = 0.00;
      day.hotelCost = 0;
      day.attractionCost = 0;
      day.transportCost = 0;
      day.foodCost = 0;
      var events = day.events.filter((item) => {
        return item.hidden != true;
      });     
      this.slides[this.slides.length -1 ]["events"] = events;
      day.events.forEach ( event => {
        if (event.duration){
          var duration = moment.duration(event.duration);
          event.duration = moment.utc(duration.asMilliseconds()).format("HH:mm").toString();
          
        } else{
          event.duration = "--:--";
        }
        if (event.totalPriceCurrencyCode != undefined && event.totalPriceCurrencyCode != ""){
          day.totalPriceCurrencyCode = event.totalPriceCurrencyCode;
        }
        if (event.totalPrice === undefined){
          day.totalCost += 0.00; 
        } else{
          event.totalPrice = event.totalPrice/100;
          day.totalCost = day.totalCost + event.totalPrice;
          if (event.kind == "TripPlanDirectionEvent"){
            day.transportCost += event.totalPrice;
          } else if (event.kind == "TripPlanAccommodationEvent"){
            day.hotelCost += event.totalPrice;
          } else if (event.eventType == "RESTAURANT"){
            day.foodCost += event.totalPrice;
          } else{
            day.attractionCost += event.totalPrice;
          }
        }
        if (event.timestampStart){
          event.startTime = moment(event.timestampStart).format("HH:mm");
        } else{
          event.startTime = "--:--";
        }
        
        if (event.startTime == "Invalid date"){
          event.startTime = "--:--"
        }
      });
      firstDay = firstDay.add(1,'days');
    });
    console.log(this.tripDays);
    this.loader.presentDismiss();
  }
  
  onSegmentChanged(segmentButton) {
    const selectedIndex = this.slides.findIndex((slide) => {
      return slide.id == segmentButton.value;
    });
    this.slider.slideTo(selectedIndex);
  }

  onSlideChanged(slider) {
    const currentSlide = this.slides[slider._activeIndex];
    if (currentSlide != undefined){
      this.selectedSegment = currentSlide.id.toString();
      this.slider.slideTo(currentSlide.id);
      this.updateScroll(currentSlide.id);
    }
  }

  private updateScroll(slideId){    
    let elementPosition = this.scroll._scrollContent.nativeElement.children.item(0).children.item(1).children.item(slideId).offsetLeft;
    let currentScroll = this.scroll._scrollContent.nativeElement.scrollLeft;
    if (elementPosition - currentScroll  > 250){
      let elementPositionBefore = this.scroll._scrollContent.nativeElement.children.item(0).children.item(1).children.item(slideId-2).offsetLeft;
      this.scroll._scrollContent.nativeElement.scrollTo({ left: elementPositionBefore, top: 0, behavior: 'smooth' });
    }
    else if (currentScroll > elementPosition){
      if (elementPosition < this.scrollOffset){
        this.scroll._scrollContent.nativeElement.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
      } else{
        let elementPositionAfter = this.scroll._scrollContent.nativeElement.children.item(0).children.item(1).children.item(slideId-2).offsetLeft;
        this.scroll._scrollContent.nativeElement.scrollTo({ left: elementPositionAfter, top: 0, behavior: 'smooth' });
      }
      
    } else 
    {
      if (elementPosition < this.scrollOffset){
        this.scroll._scrollContent.nativeElement.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
      } else{
        let elementPositionAfter = this.scroll._scrollContent.nativeElement.children.item(0).children.item(1).children.item(slideId-2).offsetLeft;
        this.scroll._scrollContent.nativeElement.scrollTo({ left: elementPositionAfter, top: 0, behavior: 'smooth' });
      }
    }
  }

  private TripPlanAccommodationEvent(event){
    if (event.kind == "TripPlanAccommodationEvent"){
      return true;
    }
  }

  private isTripPlanEvent(event){
    if (event.kind == "TripPlanAttractionEvent"){
      return true;
    }
  }

  private isTripEstablishmentEvent(event){
    if (event.kind == "TripPlanEstablishmentEvent"){
      return true;
    }
  }

  private isTripDirectionEvent(event){
    if (event.kind == "TripPlanDirectionEvent"){
      return true;
    }
  }

  private isRestaurantEvent(event){
    if (event.eventType == "RESTAURANT"){
      return true;
    }
  }

  private isBicycle(event){
    if (event.meansOfTransportation == "BICYCLE"){
      return true;
    }
  }

  private convertTimestampToHourMinute(dateTimestamp) {
    var dateLabel,
      hours,
      minutes;
    var MINUTE = 60 * 1000;
    var HOUR = 60 * MINUTE;
    var DAY = 24 * HOUR;

    hours = Math.floor(dateTimestamp / HOUR);
    minutes = (dateTimestamp % HOUR) / MINUTE;
    return hours + ":" + minutes + "0";
  };

  private openAttraction(event){
    this.navCtrl.push(AttractioncardPage, { data: event});
  }
  private openDirection(event){
    this.navCtrl.push(DirectioncardPage, { data: event});
  }

  private openEstablishment(event)
  {
    this.navCtrl.push(EstablishmentcardPage, { data: event })
  }

  private openAccomodation(event){
    this.navCtrl.push(AccomodationcardPage, { data: event});
  }

  openSummary() {
    let modal = this.modalCtrl.create(DaySummaryPage,  {dayDetails: this.tripDays[parseInt(this.selectedSegment)], 
                                                        dayNumber: this.slides[parseInt(this.selectedSegment)].day,
                                                        dayIndex: parseInt(this.selectedSegment)});
      modal.present();
      modal.onWillDismiss((data: any[]) => {
      if (data) {

      }
      });
  }

  ionViewDidLoad() {
    this.widthScroll = document.getElementById('segment-test').scrollWidth;
  }


}
