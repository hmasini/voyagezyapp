import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TripDetailsPage } from './trip-details';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    TripDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(TripDetailsPage),
    SuperTabsModule,
    TranslateModule.forChild()
  ],
})
export class TripDetailsPageModule {}
