import { Component } from '@angular/core';
import { IonicPage, NavController,ModalController, LoadingController, AlertController, ToastController} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { MainPage } from '../pages';
import { ForgetPasswordPage } from "../pages";
import { AuthService } from '../../providers/auth/authservice';
import { UserService } from '../../providers/user/userservice';

/**
 * The Welcome Page is a splash page that quickly describes the app,
 * and then directs the user to create an account or log in.
 * If you'd like to immediately put the user onto a login/signup page,
 * we recommend not using the Welcome page.
*/
@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})
export class WelcomePage {
  loginForm: FormGroup;
  loader: any;
  account: { email: string, password: string } = {
    email: '',
    password: ''
  };

  // Our translated text strings
  private loginErrorString: string;
  constructor(public navCtrl: NavController, public loadingController: LoadingController, public formBuilder: FormBuilder, 
    public toastCtrl: ToastController, public alertCtrl: AlertController, public modalCtrl: ModalController, public storage: Storage,
    public userService: UserService, public authService: AuthService) { 
    
    this.getAuthUser();

    this.loginForm = formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})')
      ])),
      password: new FormControl('', Validators.compose([
          Validators.required
      ])),
    });

  }

  private getAuthUser() {
    this.userService.getOnStorage().then(
      (user) => {
        if (user != null)
          this.navCtrl.setRoot(MainPage);
      }
    );
  }

  login() {
    this.account.email = this.loginForm.controls.email.value;
    this.account.password = this.loginForm.controls.password.value;
    this.loader = this.loadingController.create({
      content: "Please wait"
    });  
    if(this.loginForm.valid){
      this.loader.present();
      this.authService.login(this.account).then(
        (user) => {
          this.userService.createOnStorage(user);
          this.loader.dismiss();
          this.navCtrl.setRoot(MainPage);
        }, 
        (error) => {
          let alert = this.alertCtrl.create({
            title: "Incorrect Login",
            subTitle: error,
            buttons: [
              {
                text: 'OK',
                role: 'ok',
              }
            ],
    
          });
          alert.present();
          this.loader.dismiss(); 
        }
      );
    }else{
      let alert = this.alertCtrl.create({
        title: "Incorrect Login",
        subTitle: "Please fill in the fields correctly.",
        buttons: [
          {
            text: 'OK',
            role: 'ok',
          }
        ],

    });
    alert.present();
    }

  }

  
  recoverUserPassword(){
    let modal = this.modalCtrl.create(ForgetPasswordPage);
    modal.present();
    modal.onWillDismiss((data: any[]) => {
        if (data) {

        }
    });
  }

}
