// The page the user lands on after opening the app and without a session
export const FirstRunPage = 'WelcomePage';

// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
export const MainPage = 'TabsPage';

// The initial root pages for our tabs (remove if not using tabs)
export const Tab1Root = 'TripsPage';
export const Tab2Root = 'MessagesPage';
export const Tab3Root = 'AccountPage';
export const TripsPage = 'TripsPage';
export const TripDetailsPage = 'TripDetailsPage';
export const AttractioncardPage = 'AttractioncardPage';
export const DirectioncardPage = 'DirectioncardPage';
export const AccomodationcardPage = 'AccomodationcardPage';
export const EstablishmentcardPage = 'EstablishmentcardPage';
export const MessagesPage = 'MessagesPage';
export const MessagedetailsPage = "MessagedetailsPage";
export const WelcomePage = "WelcomePage";
export const ForgetPasswordPage = "ForgetPasswordPage";
export const DaySummaryPage = "DaySummaryPage";
export const LanguagePage = "LanguagePage";