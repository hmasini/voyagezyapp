import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserService } from '../../providers/providers';
import { User } from '../../models/user';
import { TranslateService } from '@ngx-translate/core';

declare var google;
declare var geocoder;
/**
 * Generated class for the DirectioncardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-directioncard',
  templateUrl: 'directioncard.html',
})
export class DirectioncardPage {

  private map: any;
  private tripEvent: any;
  private pointA: any;
  private pointB: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserService, public translate: TranslateService) {
    this.userService.getOnStorage().then((user) => {
      this.translate.use(User.ParseFromObject(user).language);
    });
    this.tripEvent = navParams.get('data');
    console.log(this.tripEvent);
  }

  ionViewDidLoad() {
     this.loadMap();
  }
  
  //initialize google maps at element #map 
  loadMap(){
    
    var latlng = new google.maps.LatLng(-34.397, 150.644);
    var myOptions = {
      zoom: 7,
      center: latlng,
      mapTypeControl: true,
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
      },
      navigationControl: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.getElementById("map"), myOptions);
    this.buildMap(this.tripEvent.origin, this.tripEvent.destination, map, this.tripEvent.meansOfTransportation);
  }

  private buildMap(origin, destination, map, travelMode)
  {
    if((origin.hasOwnProperty("latitude")) && (origin.hasOwnProperty("longitude")) &&
     (destination.hasOwnProperty("latitude")) && (destination.hasOwnProperty("longitude"))){
      // starting direction service
      var directionsService = new google.maps.DirectionsService,
      directionsDisplay = new google.maps.DirectionsRenderer({
          map: map,
          suppressBicyclingLayer: true
      });
      var ltlngA = new google.maps.LatLng(origin.latitude, origin.longitude);
      var ltlngB = new google.maps.LatLng(destination.latitude, destination.longitude);
      var travelMode;
      if (travelMode == "SUBWAY"){
        travelMode = google.maps.TravelMode.TRANSIT;
      } else if (travelMode == "WALKING"){
        travelMode = google.maps.TravelMode.WALKING;
      } else{
        travelMode = google.maps.TravelMode.BICYCLING;
      }
      directionsService.route({
        origin: ltlngA,
        destination: ltlngB,
        travelMode: travelMode
      }, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(response);
        } else {
          window.alert('Directions request failed due to ' + status);
        }
      });
    }
  }

  private geocodeOrigin(origin, destination, map, travelmode){
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({
      'address': origin
    }, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
          var pointA = results[0].geometry.location;
          map.setCenter(results[0].geometry.location);
          geocoder.geocode({
            'address' : destination
          }, function(results, status){
            if (status == google.maps.GeocoderStatus.OK) {
              if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                var pointB = results[0].geometry.location;
                var directionsService = new google.maps.DirectionsService,
                directionsDisplay = new google.maps.DirectionsRenderer({
                   map: map
                });
                var ltlngA = new google.maps.LatLng(pointA.lat(), pointA.lng());
                var ltlngB = new google.maps.LatLng(pointB.lat(), pointB.lng());
                var travelMode;
                if (travelmode == "SUBWAY"){
                  travelMode = google.maps.TravelMode.TRANSIT;
                } else if (travelmode == "WALKING"){
                  travelMode = google.maps.TravelMode.WALKING;
                } else{
                  travelMode = google.maps.TravelMode.BICYCLING;
                }
                directionsService.route({
                  origin: ltlngA,
                  destination: ltlngB,
                  travelMode: travelMode
                }, function(response, status) {
                  if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                  } else {
                    window.alert('Directions request failed due to ' + status);
                  }
                });
              }
            }
          });


        } else {
          alert("No results found");
        }
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
  }
    
  private calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB) {

  }
}
