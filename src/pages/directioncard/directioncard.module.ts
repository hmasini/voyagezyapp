import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DirectioncardPage } from './directioncard';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    DirectioncardPage,
  ],
  imports: [
    IonicPageModule.forChild(DirectioncardPage),
    TranslateModule.forChild(),
  ],
})
export class DirectioncardPageModule {}
