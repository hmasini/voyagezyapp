import { Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, LoadingController, AlertController, NavParams } from 'ionic-angular';
import { Trip } from '../../providers/providers';
import { Trips } from '../../providers/providers';
import { Loader } from '../../providers/providers';
import { UserService } from '../../providers/providers';
import { TranslateService } from '@ngx-translate/core';
import { TripDetailsPage } from '../pages';
import {App} from 'ionic-angular';
import { User } from '../../models/user';
import { WelcomePage } from '../pages';
/**
 * Generated class for the TripsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-trips',
  templateUrl: 'trips.html',
})
export class TripsPage {
  private allTrips: Array<any>;
  private loaded: Boolean = false;
  
  constructor(public navCtrl: NavController,  public app: App, loadingController: LoadingController, public alertCtrl: AlertController, 
              public navParams: NavParams, public trip: Trip, public trips: Trips, public loader: Loader, 
              public userService: UserService, public translate: TranslateService) {

    this.userService.getOnStorage().then((user) => {
      this.translate.use(User.ParseFromObject(user).language);
      this.loader.presentLoadingCustom();
      this.trips.retrieveTrips().then((resp : any) => {
          console.log(resp);
          window.localStorage.setItem("trips", JSON.stringify(resp)); 
          this.allTrips  = JSON.parse(window.localStorage.getItem("trips"));
          this.loader.presentDismiss();
      },
      (err) => {
        console.log(err);
        if (err.status == 401)
        {
          this.userService.deleteOnStorage().then(() => {
            this.app.getRootNav().setRoot(WelcomePage);
          });
        }
      });
    });
    this.loaded = true;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TripsPage');
  }

  ionViewWillEnter(){

    this.userService.getOnStorage().then((user) => {
      this.translate.use(User.ParseFromObject(user).language);
    });
  }

  private openDetail(item){
    if (item.status != "WAITING_PAYMENT"){
        this.loader.presentLoadingCustom();
        this.trip.tripDetails(item._id).then((resp : any) => {
          window.localStorage.setItem("trip-" + item._id, JSON.stringify(resp));
          this.navCtrl.push(TripDetailsPage, { data: "trip-" + item._id});
        },
        (err) => {
          console.log(err);
        });
    } else{
      let alert = this.alertCtrl.create({
        title: "Aguardando pagamento",
        subTitle: "O pagamento deve ser realizado através da plataforma web.",
        buttons: [
          {
            text: 'OK',
            role: 'ok',
          }
        ],

    });
    alert.present();
    }
  }
}
