import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, Tabs} from 'ionic-angular';
import { TripsPage } from '../pages';
import { MessagesPage } from '../pages';
import { Trips } from '../../providers/providers';

/**
 * Generated class for the MasterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-master',
  templateUrl: 'master.html',
})
export class MasterPage {

  constructor(private app: App, public navCtrl: NavController, public navParams: NavParams, public trips: Trips) {
    console.log("Here master");
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad MasterPage');
    // this.trips.retrieveTrips().subscribe((resp : any) => {
    //   window.localStorage.setItem("trips", JSON.stringify(resp));
    // },
    // (err) => {
    //   console.log(err);
    // });
  }

  private openTrips(){
    const tabsNav = this.app.getNavByIdOrName('myTabsNav') as Tabs;
    tabsNav.select(1);
  }
  private openMessages(){
    const tabsNav = this.app.getNavByIdOrName('myTabsNav') as Tabs;
    tabsNav.select(2);
  }
}
