import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Chart } from 'chart.js';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from '../../providers/providers';
import { User } from '../../models/user';

/**
 * Generated class for the DaysummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-daysummary',
  templateUrl: 'daysummary.html',
})
export class DaySummaryPage {
  @ViewChild('doughnutCanvas') doughnutCanvas;
  doughnutChart: any;
  dayDetails: any;
  dayIndex: any;
  dayNumber: any;
  attractionLabel: string;
  transportLabel: string;
  foodLabel: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, 
              public userService: UserService, public translate: TranslateService) {
    this.userService.getOnStorage().then((user) => {
        this.translate.use(User.ParseFromObject(user).language);
        this.translate.get('ATTRACTIONS_CHART_LABEL').subscribe(
          value => {
            // value is our translated string
            this.attractionLabel = value;
          }
        )
        this.translate.get('TRANSPORT_CHART_LABEL').subscribe(
          value => {
            // value is our translated string
            this.transportLabel = value;
          }
        )
        this.translate.get('FOOD_CHART_LABEL').subscribe(
          value => {
            // value is our translated string
            this.foodLabel = value;
          }
        )
    });
    this.dayDetails = navParams.get('dayDetails');
    this.dayIndex = navParams.get('dayIndex');
    this.dayNumber = navParams.get('dayNumber');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DaysummaryPage');
  }

  ionViewDidEnter(){
    if (this.dayDetails.totalCost > 0){
      this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
        type: 'doughnut',
        cutoutPercentage: 0,
        data: {
            labels: ["Hotel (" + this.dayDetails.hotelCost.toFixed(2) + ")", this.attractionLabel + " (" + this.dayDetails.attractionCost.toFixed(2) + ")", 
                      this.transportLabel + " (" + this.dayDetails.transportCost.toFixed(2) + ")", this.foodLabel + " (" + this.dayDetails.foodCost.toFixed(2) + ")"],
            datasets: [{
                label: 'Total Price',
                data: [this.dayDetails.hotelCost, this.dayDetails.attractionCost, this.dayDetails.transportCost, this.dayDetails.foodCost],
                backgroundColor: [
                    'rgba(66,134,244, 0.5)',
                    'rgba(244,65,65, 0.5)',
                    'rgba(244,238,65, 0.5)',
                    'rgba(65,244,85, 0.5)',
                ],
                hoverBackgroundColor: [
                    "#4286f4",
                    "#f44141",
                    "#f4ee41",
                    "#41f455",
                ]
            }]
        },
        options: {
          cutoutPercentage: 0, // pie chart,
          responsive: true,
          maintainAspectRatio: false,
          legend: {
            position: 'bottom',
            padding: 20,
            display: true,
            fullWidth: true,
            onClick: () => {}, //prevent filter by default
            labels: {
              generateLabels: (chart) => {
      
                chart.legend.afterFit = function () {
                  var width = this.width;                  
                  this.lineWidths = this.lineWidths.map( () => this.width-12 );
                  
                  this.options.labels.padding = 20;
                  this.options.labels.boxWidth = 15;
                };
      
                var data = chart.data;
                //https://github.com/chartjs/Chart.js/blob/1ef9fbf7a65763c13fa4bdf42bf4c68da852b1db/src/controllers/controller.doughnut.js
                if (data.labels.length && data.datasets.length) {
                  return data.labels.map((label, i) => {
                    var meta = chart.getDatasetMeta(0);
                    var ds = data.datasets[0];
                    var arc = meta.data[i];
                    var custom = arc && arc.custom || {};
                    var getValueAtIndexOrDefault = this.getValueAtIndexOrDefault;
                    var arcOpts = chart.options.elements.arc;
                    var fill = custom.backgroundColor ? custom.backgroundColor : getValueAtIndexOrDefault(ds.backgroundColor, i, arcOpts.backgroundColor);
                    var stroke = custom.borderColor ? custom.borderColor : getValueAtIndexOrDefault(ds.borderColor, i, arcOpts.borderColor);
                    var bw = custom.borderWidth ? custom.borderWidth : getValueAtIndexOrDefault(ds.borderWidth, i, arcOpts.borderWidth);
                    
                    return {
                      text: label,
                      fillStyle: fill,
                      strokeStyle: stroke,
                      lineWidth: bw,
                      hidden: isNaN(ds.data[i]) || meta.data[i].hidden,
      
                      // Extra data used for toggling the correct item
                      index: i
                    };
                  });
                }
                return [];
              }
            }
          }
        }
    });
    }
  }

isArray = Array.isArray ?
  function (obj) {
    return Array.isArray(obj);
  } :
  function (obj) {
    return Object.prototype.toString.call(obj) === '[object Array]';
  };

getValueAtIndexOrDefault = (value, index, defaultValue) => {
  if (value === undefined || value === null) {
    return defaultValue;
  }

  if (this.isArray(value)) {
    return index < value.length ? value[index] : defaultValue;
  }

  return value;
};

  dismiss(data?: any) {
    this.viewCtrl.dismiss();
  }

}
