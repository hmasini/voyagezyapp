import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DaySummaryPage } from './daysummary';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [
    DaySummaryPage,
  ],
  imports: [
    IonicPageModule.forChild(DaySummaryPage),
    TranslateModule.forChild(),
  ],
})
export class DaySummaryPageModule {}
