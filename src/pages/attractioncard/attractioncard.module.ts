import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AttractioncardPage } from './attractioncard';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AttractioncardPage,
  ],
  imports: [
    IonicPageModule.forChild(AttractioncardPage),
    TranslateModule.forChild(),
  ],
})
export class AttractioncardPageModule {}
