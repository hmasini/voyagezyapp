import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserService } from '../../providers/providers';
import { User } from '../../models/user';
import { TranslateService } from '@ngx-translate/core';
/**
 * Generated class for the AttractioncardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-attractioncard',
  templateUrl: 'attractioncard.html',
})
export class AttractioncardPage {
  private tripEvent: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public userService: UserService, 
              public translate: TranslateService) {
    this.userService.getOnStorage().then((user) => {
        this.translate.use(User.ParseFromObject(user).language);
    });
    this.tripEvent = navParams.get('data');
    if((this.tripEvent.imageUrl == null) || (this.tripEvent.imageUrl == undefined))
      this.tripEvent.imageUrl = 'assets/img/default-image-bk.png';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AttractioncardPage');
  }

  

}
