import { Component } from '@angular/core';
import { IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';
import { Messages } from '../../providers/providers';
import { MessagedetailsPage } from '../pages';
import { Loader } from '../../providers/providers';
import { UserService } from '../../providers/providers';
import { Observable } from 'rxjs/Observable';
import {Message} from '../../models/message';
import {Conversation} from '../../models/conversation';
import {App} from 'ionic-angular';
import { WelcomePage } from '../pages';
import { TranslateService } from '@ngx-translate/core';
import moment from 'moment';
import { User } from '../../models/user';
/**
 * Generated class for the MessagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html',
})

export class MessagesPage {
  private conversations: Array<Conversation>;
  private loaded: Boolean = false;
  private trips: any;
  private conversation: any;
  private messageObservables: any;
  constructor(public navCtrl: NavController, public app: App, 
              public navParams: NavParams, public messages: Messages, public loader: Loader, 
              public userService: UserService, public translate: TranslateService) {
    this.trips = JSON.parse(window.localStorage.getItem("trips"));
    this.conversations = [];
    this.messageObservables = [];
    if (this.trips){
      this.userService.getOnStorage().then((user) => {
        this.translate.use(User.ParseFromObject(user).language);
        this.trips.forEach(trip => {
          this.messages.retrieveMessages(trip.conversation).then((res : any) => {
            var conversation = new Conversation(trip.conversation, trip.title, trip.expertUser.firstname, 
                                                trip.expertUser.lastname, trip.expertUser.imageUrl, 
                                                res.messages, res.messages[res.messages.length - 1]);
            this.conversations.push(conversation);
            this.conversations.sort((a,b) => {
              return this.sortMessages(a,b);
            });
        },
        (err) => {
          if (err.status == 401)
          {
            this.userService.deleteOnStorage().then(() => {
              this.app.getRootNav().setRoot(WelcomePage);
            });
          }
        });
        });
      });
      this.loaded = true;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessagesPage');
  }

  ionViewWillEnter(){
    if (this.trips != null){
      this.trips.forEach(trip => {
        this.messages.retrieveMessages(trip.conversation).then((res : any) => {
          var conversationIndex = this.conversations.findIndex(x => x.getId() == trip.conversation);
          if (conversationIndex != -1 )
            this.conversations[conversationIndex].setLastMessage(res.messages[res.messages.length - 1]);
          this.messageObservables.push(this.messages.getMessages(trip.conversation).subscribe(message => {
            var conversationIndex = this.conversations.findIndex(x => x.getId() == trip.conversation);
            this.conversations[conversationIndex].pushMessage(message);
            this.conversations[conversationIndex].setLastMessage(message);
            this.conversations.sort((a,b) => {
              return this.sortMessages(a,b);
            });
          }));
        });
      });
    }
  }

  ionViewWillLeave(){
    this.messageObservables.forEach(element => {
      
      element.unsubscribe();
    });
  }

  ionViewDidEnter() {
    console.log('did Enter');
   }

  sortMessages(a, b){
    if (moment(a.lastMessage.timestamp, "DD-MM-YYY - HH:mm") > moment(b.lastMessage.timestamp,  "DD-MM-YYY - HH:mm"))
      return -1;
    if (moment(a.lastMessage.timestamp,  "DD-MM-YYY - HH:mm") < moment(b.lastMessage.timestamp, "DD-MM-YYY - HH:mm"))
      return 1;
    return 0;
  }

  private openMessages(item){
    this.navCtrl.push(MessagedetailsPage, {data: item});
  }


}
