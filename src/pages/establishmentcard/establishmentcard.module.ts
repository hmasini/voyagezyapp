import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EstablishmentcardPage } from './establishmentcard';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    EstablishmentcardPage,
  ],
  imports: [
    IonicPageModule.forChild(EstablishmentcardPage),
    TranslateModule.forChild(),
  ],
})
export class EstablishmentcardPageModule {}
