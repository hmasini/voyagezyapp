import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, ViewController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../../providers/auth/authservice';

/**
 * Generated class for the ForgetpasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgetpassword',
  templateUrl: 'forgetpassword.html',
})
export class ForgetPasswordPage {
  passwordForm: FormGroup;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder,
              public viewCtrl: ViewController, public alertCtrl: AlertController, public authService: AuthService, 
              public toastCtrl: ToastController) {
    this.passwordForm = formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})')
      ]))
  });
  }

  dismiss(data?: any) {
    this.viewCtrl.dismiss();
  }

  send(){
    if(this.passwordForm.valid){
      console.log(this.passwordForm.controls.email.value);
      this.authService.forgotpassword(this.passwordForm.controls.email.value).then((resp : any) => {
        console.log(resp);
        if (resp.state !== 'success') {
            // Unable to log in
            let toast = this.toastCtrl.create({
              message: "There was a problem with your request",
              duration: 3000,
              position: 'top'
            });
            toast.present();
            return;
        } 
        // Setting token in local storage
        this.dismiss();
      }, (err) => {
        // Unable to log in
        let toast = this.toastCtrl.create({
          message: "teste",
          duration: 3000,
          position: 'top'
        });
        toast.present();
      });
    }
    else{
      let alert = this.alertCtrl.create({
        title: "Incorrect e-mail",
        subTitle: "Please fill in a valid e-mail address",
        buttons: [
          {
            text: 'OK',
            role: 'ok',
          }
        ],

    });
    alert.present();
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgetpasswordPage');
  }

}
