import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController, ModalController, NavParams } from 'ionic-angular';
import { Events } from 'ionic-angular';
import { UserService } from '../../providers/providers';
import { WelcomePage } from '../pages';
import { Loader } from '../../providers/providers';
import { Storage } from '@ionic/storage';
import { User } from '../../models/user';
import { LanguagePage } from "../pages";
import { TranslateService } from '@ngx-translate/core';
import {App} from 'ionic-angular';
/**
 * Generated class for the AccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {
  private user : User;
  private firstname : string;
  private lastname : string;
  private email : string;
  private image : string;
  private language : string;
  private languages: Array<{name: string, value: string, isChecked: boolean}>;
  constructor(public navCtrl: NavController, public app: App, public navParams: NavParams, 
              public toastCtrl: ToastController, public events: Events, public userService: UserService, 
              private storage: Storage, public loader: Loader, public modalCtrl: ModalController, public translate: TranslateService) {
      //this.loader.presentLoadingCustom();
      this.userService.getOnStorage().then((res) => {
        this.user = User.ParseFromObject(res);
        this.firstname = this.user.firstname;
        this.lastname = this.user.lastname;
        this.email = this.user.email;
        this.image = this.user.imageUrl;
        this.language = this.user.language;
        this.languages = new Array();
        this.languages.push({name: "English", value: "en", isChecked: false}, 
                            {name: "Portuguese", value: "pt-br", isChecked: false});
        this.languages.forEach((language) =>{
          if (language.value == this.language)
            language.isChecked = true;
        });    
        this.translate.use(this.language);
      }, (error) => console.log("something went wrong"));
                
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');
  }

  // Attempt to login in through our User service
  doLogout() {
    // Setting token in local storage
    this.userService.deleteOnStorage().then(() => {
    // Unable to log in
      let toast = this.toastCtrl.create({
        message: "Logout realizado com sucesso",
        duration: 3000,
        position: 'top'
      });
      toast.present();
      this.app.getRootNav().setRoot(WelcomePage);
    });
  }

  changeLanguage()
  {
    let modal = this.modalCtrl.create(LanguagePage,  {languages: this.languages});
    modal.present();
    modal.onWillDismiss((data) => {
    if (data) {
      this.languages.forEach((language) =>{
        if (language.value == data)
          language.isChecked = true;
        else
          language.isChecked = false;
      });
      this.translate.use(data);
    }
    });
  }

}
