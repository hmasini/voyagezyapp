import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccountPage } from './account';
import { StarRatingModule } from 'ionic3-star-rating';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AccountPage,
  ],
  imports: [
    StarRatingModule,
    IonicPageModule.forChild(AccountPage),
    TranslateModule.forChild()
  ],
})
export class AccountPageModule {}
