import { Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, Config, Content, NavParams } from 'ionic-angular';
import { Socket } from 'ng-socket-io';
import { Observable } from 'rxjs/Observable';
import { Messages } from '../../providers/providers';
import {Conversation} from '../../models/conversation';
/**
 * Generated class for the MessagedetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-messagedetails',
  templateUrl: 'messagedetails.html',
})
export class MessagedetailsPage {
  @ViewChild('content') content:any;

  private conversation: Conversation;
  private messageField: string;
  private messageObservable: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private socket: Socket, private messages: Messages,
              private config: Config) {
    this.config.set("ios", "scrollPadding", true);
    this.config.set("ios", "scrollAssist", false);
    this.config.set("ios", "autoFocusAssist", true);
    this.config.set("android", "scrollPadding", true );
    this.config.set("android", "scrollAssist", true );
    this.config.set("android", "autoFocusAssist", true);
    this.conversation = navParams.get('data');
    this.messageObservable = this.messages.getMessages(this.conversation.getId()).subscribe(message => {
      this.conversation.pushMessage(message);
      setTimeout(() => {
        //this.content.scrollToBottom(0);//300ms animation speed
      });
    });
  }

  ionViewDidEnter(){
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessagedetailsPage');
  }

  ionViewWillLeave(){
    this.messageObservable.unsubscribe();
    console.log("here leave");
  }

  callFunction(){
    this.content.scrollToBottom(0);
  }

  private sendMsg()
  {
    this.messages.emit(this.conversation.getId(), this.messageField);
    var message = {
      text: this.messageField,
      timestamp: new Date(),
      personKind: 'RegularUser',
      _id: ''
    }
    this.conversation.pushMessage(message);
    setTimeout(() => {
      this.content.scrollToBottom(0); //300ms animation speed
    });
    this.messageField = "";
  }

}
