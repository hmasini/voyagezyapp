import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams } from 'ionic-angular';
import { UserService } from '../../providers/providers';
import { User } from '../../models/user';
/**
 * Generated class for the LanguagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-language',
  templateUrl: 'language.html',
})
export class LanguagePage {

  languages: Array<any>;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public userService: UserService) {
    this.languages = this.navParams.data["languages"];
    console.log(this.languages);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LanguagePage');
  }

  toggleLanguage(value){
    this.userService.updateLanguageOnStorage(value).then((user) =>{
      this.viewCtrl.dismiss(User.ParseFromObject(user).language);
    }, 
    (err) => {
      console.log("something went wrong");
    });
  }

  dismiss(data?: any) {
    this.viewCtrl.dismiss();
  }

}
