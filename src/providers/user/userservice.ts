import 'rxjs/add/operator/toPromise';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Storage } from '@ionic/storage';
import { User } from '../../models/user';

@Injectable()
export class UserService {
  private _user: Subject<User> = new Subject<User>();

  constructor(private storage: Storage) {

  }

  /* ---------------------------------------------------------------------------------------------------------------- */
  /* Observable use object                                                                                            */

  public subscribeToUserService(callback) {
    return this._user.subscribe(callback);
  }

  public updateUserService(user: User) {
    this._user.next(user);
  }

  /* ---------------------------------------------------------------------------------------------------------------- */
  /* User storage management                                                                                          */

   /**
   * Write user properties in the local storage.
   *
   * @param user
   * @returns {Promise<User>}
   */
  createOnStorage(user: User): Promise<User> {
    return new Promise((resolve) => {
      this.getOnStorage().then((res) => {
        if (res) {
          this.deleteOnStorage().then(() => {

          });
        }
      }).then(() => {
        this.updateUserService(user);
        this.storage.set('user', user);
        resolve();
      });
    });
  }

  /**
   * Get user properties from local storage.
   *
   * @returns {Promise<User>}
   */
  getOnStorage(): Promise<User> {
    return new Promise((resolve) => {
      this.storage.get("user").then((user) => {
        this.updateUserService(user);
      });
      resolve(this.storage.get('user'));
    });
  }

  /**
   * Update user properties from local storage.
   *
   * @param user
   * @returns {Promise<User>}
   */
  updateOnStorage(user: User): Promise<User> {
    return new Promise((resolve) => {
      resolve(this.storage.get('user'));
    });
  }

  /**
   * Update user language from local storage.
   *
   * @param user
   * @returns {Promise<User>}
   */
  updateLanguageOnStorage(language: string): Promise<User> {
    return new Promise((resolve) => {
      this.storage.get('user').then(user => {
        user._language = language;
        resolve(this.storage.set('user', user));
      });
    });
  }

  /**
   * Delete user properties from local storage.
   *
   * @returns {Promise<User>}
   */
  deleteOnStorage(): Promise<User> {
    return new Promise((resolve) => {
      this.storage.clear();
      resolve();
    });
  }
}
