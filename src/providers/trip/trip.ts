import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from '../api/apiservice';

/*
  Generated class for the TripProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class Trip {

  constructor(public apiService: ApiService) {  }

  /**
   * Send a GET request to the detailed trip
   */
  tripDetails(id : string) {
    return new Promise((resolve, reject) => {
      this.apiService.getRequest('api/trips' + '/' + id)
        .subscribe(
          res => resolve(res),
          error => reject(<any>error));
    });
  }

}
