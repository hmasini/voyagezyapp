import { Injectable } from '@angular/core';
import { LoadingController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

/*
  Generated class for the LoaderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class Loader {
  private loading: any;
  private message: any;
  constructor(public loadingController: LoadingController, public translate: TranslateService) {}

  presentLoadingCustom() {
    this.translate.get('PLEASE_WAIT').subscribe(
      value => {
        // value is our translated string
        this.message = value;
      }
    )
    this.loading = this.loadingController.create({
        spinner: 'hide',
        dismissOnPageChange: false,
        content: `
            <div class="custom-spinner-container">
              <div class="custom-spinner-box">` + this.message + `</div>
            </div>`
  });
    this.loading.present();
  }

  presentDismiss() {
      this.loading.dismiss();
  }

}
