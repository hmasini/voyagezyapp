import { Injectable } from '@angular/core';

/*
  Generated class for the WindowService provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class WindowService {
  public window;
  constructor() {
    this.window = window;
  }

}
