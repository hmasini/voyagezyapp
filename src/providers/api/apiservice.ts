import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../user/userservice'; 
import { Observable } from 'rxjs/Observable';

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class ApiService {
  private baseUrl: string = 'https://voyagezy-app.herokuapp.com/';
  private user: User;

  constructor(private userService: UserService, private http: HttpClient) {
    this.getAuthUser();
  }

  private getAuthUser() {
    this.userService.getOnStorage().then((user) => {
      if (user)
        this.user = User.ParseFromObject(user);
    });
  }

  public setUser(user){
    this.user = user;
  }

 /**
   * Perform a GET request.
   *
   * @param url
   * @param auth
   * @returns {Promise<>}
   */
  getRequest(url: string): Observable<Object> {

    var headers = new HttpHeaders().set("Authorization", this.user.token);
    
    return this.http.get(this.baseUrl + url, { headers } );
  }

  /**
   * Perform a POST request.
   *
   * @param url
   * @param auth
   * @returns {Promise<>}
   */
  postRequest(url: string, body: any): Promise<Object> {
    return this.http.post(this.baseUrl +  url, body).toPromise();
  }

}
