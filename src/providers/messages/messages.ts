import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Socket } from 'ng-socket-io';
import { ApiService } from '../api/apiservice';
import moment from 'moment';
/*
  Generated class for the Messages provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class Messages {

  constructor(public apiService: ApiService, public socket: Socket) { 
    this.socket.connect();
  }

  /**
   * Send a GET request to our messages endpoint from a conversation
   */
  retrieveMessages(id : any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.apiService.getRequest('api/conversations/' + id)
        .subscribe(
          res => resolve(res),
          error => reject(<any>error));
    });
  }

  getMessages(id) {
    let observable = new Observable(observer => {
      this.socket.on('chat-'+id, (data) => {
        observer.next(data);
      });
    })
    return observable;
  }

  emit(id, message){
    var socketname = 'chat-'+id;
    var data = {
      privateSocketName: socketname,
      privateSocketError: 'privateSocketError',
      conversationId: id,
      message: {
        text: message,
        personKind: "RegularUser",
        timestamp: ""
      }
    };
    this.socket.emit('send:message', data); 
  }

}
