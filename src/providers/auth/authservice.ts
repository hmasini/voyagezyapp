import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../user/userservice'; 
import { ApiService } from '../api/apiservice'; 
import { Observable } from 'rxjs/Observable';

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class AuthService {
  private user: User;

  constructor(private userService: UserService, private apiService: ApiService) {
    this.getAuthUser();
  }

  private getAuthUser() {
    this.userService.getOnStorage().then((user) => {
      this.user = user;
    });
  }

  /**
   * Request an authentication access.
   *
   * @param email the email of the user
   * @param password the password of the user
   * @returns {Promise<any>}
   */
  login(accountInfo: any): Promise<User> {
    return new Promise((resolve, reject) => {
      this.apiService.postRequest('auth/login', accountInfo).then((resp) => {
        if (resp["state"] == "failure")
          throw new Error(resp["message"]);
        this.apiService.setUser(User.ParseFromApi(resp));  
        resolve(User.ParseFromApi(resp));  
      })
      .catch(error => {
        reject(<any>error);
      })
    });
  }

  /**
   * Provide forgot password functionality
   *
   * @returns {Promise<any>}
   */
  forgotpassword(email): Promise<any> {
    return new Promise((resolve, reject) => {
      this.apiService.postRequest('auth/login', email).then((resp) => {
        if (resp["state"] == "failure")
          throw new Error(resp["message"]);
        this.apiService.setUser(User.ParseFromApi(resp));  
        resolve(User.ParseFromApi(resp));  
      })
      .catch(error => {
        reject(<any>error);
      })
    });
  }

  /**
   * Logout a user from the authentication process.
   *
   * @returns {Promise<any>}
   */
  logout(): Promise<any> {
    return new Promise((resolve) => {
      this.userService.deleteOnStorage().then(() => {
        resolve();
      });
    });
  }


  /**
   * Check whether a user is already logged in.
   *
   * @returns {boolean}
   */
  isLoggedIn() {
    if (this.user.token) {
      return true;
    } else {
      return false;
    }
  }

}
