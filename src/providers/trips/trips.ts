import { Injectable } from '@angular/core';
import { ApiService } from '../api/apiservice';

/*
  Generated class for the TripsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class Trips {

  constructor(public apiService: ApiService) {  }

  /**
   * Send a GET request to our trips endpoint
   */
  retrieveTrips(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.apiService.getRequest('api/trips')
        .subscribe(
          res => resolve(res),
          error => reject(<any>error));
    });
  }

}
