

export class User {
  
  private _firstname : string;
  private _lastname : string;
  private _imageUrl : string;
  private _isRegular : boolean;
  private _status : string; 
  private _email : string;
  private _token : string;
  private _language : string;

  public static GetNewInstance(): User {
    return new User(null, null, null, null, null, null, null, null)
  }

  public static ParseFromObject(object, accountInfo = null): User {
    const model = User.GetNewInstance();
    if (object) {
      model.firstname = object._firstname;
      model.lastname = object._lastname;
      model.imageUrl = object._imageUrl;
      model.status = object._status;
      model.isRegular = object._isRegular;
      model.email = object._email;
      model.token = object._token;
      model.language = object._language;
    }
    if (accountInfo){
      model.email = accountInfo.email;
    }
    console.log(model);
    return model;
  }

  public static ParseFromApi(object): User {
    const model = User.GetNewInstance();
    if (object) {
      model.firstname = object.user.firstname;
      model.lastname = object.user.lastname;
      model.status = object.user.status;
      model.isRegular = object.user.isRegular;
      model.imageUrl = object.user.imageUrl;
      model.email = object.user.email;
      model.token = object.token;
      model.language = "pt-br";
    }
    return model;
  }

  constructor(firstname, lastname, email, imageUrl, isRegular, status, token, language) {
    this._firstname = firstname;
    this._lastname = lastname;
    this._imageUrl = imageUrl;
    this._isRegular = isRegular;
    this._status = status;
    this._email = email;
    this._token = token;
    this._language = language;
  }

  get firstname(): string {
    return this._firstname;
  }

  set firstname(value: string){
    this._firstname = value;
  }

  get lastname(): string {
    return this._lastname;
  }

  set lastname(value: string){
    this._lastname = value;
  }

  get language(): string {
    return this._language;
  }

  set language(value: string){
    this._language = value;
  }

  get status(): string {
    return this._status;
  }

  set status(value: string){
    this._status = value;
  }

  get imageUrl(): string {
    return this._imageUrl;
  }

  set imageUrl(value: string){
    this._imageUrl = value;
  }

  get isRegular(): boolean {
    return this._isRegular;
  }

  set isRegular(value: boolean){
    this._isRegular = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string){
    this._email = value;
  }

  get token(): string{
    return this._token;
  }

  set token(value: string){
    this._token = value;
  }

}