import moment from 'moment';

export class Message {
  
  private timestamp : string;
  private id : string;
  private text : string;
  private personKind :string;
  private isChangeRequestMessage : boolean;

  constructor(id, timestamp, text, personKind, isChangeRequestMessage) {
    this.timestamp = moment(timestamp).format("DD-MM-YYYY - HH:mm");
    this.id = id;
    this.text = text;
    this.personKind = personKind;
    this.isChangeRequestMessage = isChangeRequestMessage;
  }

  public getId(){
    return this.id;
  }

}