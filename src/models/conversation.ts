import {Message} from '../models/message';

export class Conversation {
  
    private title : string;
    private id : string;
    private firstname : string;
    private lastname : string;
    private imageUrl : string;
    private messages : Array<Message>;
    private lastMessage : Message;

    constructor(id, title, firstname, lastname, imageUrl, messages, lastMessage) {
      // assign fields to the model
      this.title = title;
      this.id = id;
      this.firstname = firstname;
      this.lastname = lastname;
      this.imageUrl = imageUrl;
      this.messages = new Array<Message>();
      messages.forEach(message => {
        this.messages.push(new Message(message._id, message.timestamp, message.text, message.personKind, message.isChangeRequestMessage));
      });
      this.lastMessage = new Message(lastMessage._id, lastMessage.timestamp, lastMessage.text, lastMessage.personKind, lastMessage.isChangeRequestMessage);
    }

    public pushMessage(message){
      this.messages.push(new Message(message._id, message.timestamp, message.text, message.personKind, message.isChangeRequestMessage));
    }

    public setLastMessage(message){
      this.lastMessage = new Message(message._id, message.timestamp, message.text, message.personKind, message.isChangeRequestMessage);;
    }

    public getMessages(){
      return this.messages;
    }

    public getId(){
      return this.id;
    }
  
}