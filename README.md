# Voyagezy app 🎮

Voyagezy mobile app. 

## Table of Contents

1. [Getting Started](#getting-started)
2. [Pre-Requisites](#prerequisites)
3. [Running in Browser](#browser)
3. [Running in Emulator](#emulator)

## <a name="getting-started"></a>Getting Started

These instructions will get you a copy of the project up and running on your local machine or mobile for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## <a name="prerequisites"></a>Prerequisites

* GIT 
* Ionic CLI 4.1.1
* NPM v6.3.0
* CORDOVA 8.0.0

## <a name="setup"></a>SETUP

Run npm install to get latest modules for this project. 

### <a name="browser"></a>Running via Browser

Here is the step by step to run the app in your web browser:

1) In your project folder, do:

npm install

2) Start ionic serve by:

ionic serve

3) Optionally you can start with ionic lab:

ionic serve --lab

### <a name="browser"></a>Running in Emulator:

Here is the step by step to run the app in your emulator:

1) Install and configure your android emulator:

You can use Android Studio or other available emulator for your platform

2) Add android platform: 

ionic cordova platform add android

3) Build for Android:

ionic cordova build android

4) Run in your device:

4.1) Check if device is active using: adb devices
4.1) If device is active, run: ionic cordova run android